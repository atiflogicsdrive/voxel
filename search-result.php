<?php include_once "includes/header.php";?>      
    <div class="contentWrapper searchResult">
        <div class="container">
            <div class="search wow fadeInDown" data-wow-duration="1s"">
                <form>
                   <div class="form-group">
                        <input type="text" class="form-control" placeholder="Cephalometric Analysis">
                   </div>
                </form>
            </div>
            <div class="resultWrapper">
                <p class="resultNumber wow fadeIn" data-wow-duration="1s">4 results found</p>
                <div class="result wow fadeInLeft" data-wow-duration="1s">
                    <h5>Cephalometric Analysis</h5>
                    <P>Our DSD team will create 2D and 3D digital smile design project for your patient and 3D digital...</P>
                    <a href="#" class="seeAll">See Details</a>
                </div>
                <div class="result wow fadeInLeft" data-wow-duration="1s">
                    <h5>Cephalometric Analysis</h5>
                    <P>Our DSD team will create 2D and 3D digital smile design project for your patient and 3D digital...</P>
                    <a href="#" class="seeAll">See Details</a>
                </div>
                <div class="result wow fadeInLeft" data-wow-duration="1s">
                    <h5>Cephalometric Analysis</h5>
                    <P>Our DSD team will create 2D and 3D digital smile design project for your patient and 3D digital...</P>
                    <a href="#" class="seeAll">See Details</a>
                </div>
                <div class="result wow fadeInLeft" data-wow-duration="1s">
                    <h5>Cephalometric Analysis</h5>
                    <P>Our DSD team will create 2D and 3D digital smile design project for your patient and 3D digital...</P>
                    <a href="#" class="seeAll">See Details</a>
                </div>
                <div class="button-row d-none show-on-md wow fadeInUp" data-wow-duration="1s">
                    <a href="#" class="btn btn-default full">Search</a>
                </div>
            </div>
        </div>
    </div>
<?php include_once "includes/footer.php";?>
