<?php include_once "includes/header.php";?>      
    <div class="contentWrapper productDetail">
        <div class="mainbg">
            <div class="container">
                <div class="offerBar wow fadeInDown" data-wow-duration="1s">
                    <div class="offerBar--text">
                        <span class="offerBar--icon">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g style="mix-blend-mode:multiply">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9754 3.92501L12.0442 4.85763C11.7481 5.15157 11.3426 5.31878 10.921 5.31878H9.58611C7.23039 5.31878 5.31957 7.23024 5.31957 9.5867V10.9191C5.31957 11.3415 5.15231 11.7463 4.85311 12.0476L3.91201 12.9899C2.24712 14.6624 2.25471 17.3648 3.92636 19.0271L4.8558 19.9579C5.15185 20.2546 5.31957 20.6605 5.31957 21.0811V22.4161C5.31957 24.7725 7.23033 26.6827 9.58611 26.6827H10.921C11.3414 26.6827 11.7473 26.8504 12.0482 27.1491L12.9892 28.0903C14.6636 29.7552 17.366 29.7476 19.0269 28.0754L19.9571 27.1465C20.2548 26.8502 20.6601 26.6827 21.0815 26.6827H22.4151C24.7719 26.6827 26.683 24.7728 26.683 22.4161V21.0811C26.683 20.6594 26.8502 20.2538 27.1459 19.9574L28.0905 19.0128C29.7549 17.3389 29.7467 14.6386 28.0767 12.9761L27.1448 12.0442C26.8497 11.7471 26.683 11.3426 26.683 10.9191V9.5867C26.683 7.22995 24.7719 5.31878 22.4151 5.31878H21.0815C20.659 5.31878 20.254 5.1518 19.9555 4.85473L19.0099 3.91036C17.3399 2.24791 14.6379 2.25492 12.9754 3.92501ZM16.9983 5.68398L17.1833 5.85186L18.072 6.74259C18.8728 7.53958 19.9543 7.98548 21.0815 7.98548H22.4151C23.2991 7.98548 24.0163 8.70272 24.0163 9.58674V10.9191C24.0163 12.0467 24.4615 13.1265 25.2558 13.9265L26.1932 14.8639C26.8224 15.4903 26.8255 16.503 26.1932 17.1389L25.2591 18.073C24.4628 18.8712 24.0163 19.9543 24.0163 21.0811V22.4161C24.0163 23.2997 23.2995 24.0161 22.4151 24.0161H21.0815C19.9547 24.0161 18.8731 24.463 18.0743 25.258L17.1386 26.1924C16.5542 26.7808 15.6327 26.8225 15.0025 26.3184L14.869 26.1989L13.9304 25.2603C13.1273 24.4628 12.0463 24.0161 10.921 24.0161H9.58607C8.70288 24.0161 7.9862 23.2996 7.9862 22.4161V21.0811C7.9862 19.9547 7.53859 18.8714 6.74318 18.0741L5.80995 17.1395C5.22151 16.5544 5.1798 15.6328 5.68348 15.0036L5.8093 14.8639L6.74268 13.9292C7.53997 13.1263 7.9862 12.0463 7.9862 10.9191V9.58674C7.9862 8.70283 8.70331 7.98548 9.58607 7.98548H10.921C12.0466 7.98548 13.1276 7.53981 13.9271 6.74593L14.8638 5.80783C15.4474 5.22165 16.3691 5.18028 16.9983 5.68398Z" fill="#EA5A43"/>
                                <g opacity="0.5">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M19.2618 12.2241C19.8745 11.8156 20.7024 11.9812 21.1108 12.5939C21.4901 13.1628 21.3745 13.9172 20.8654 14.3492L20.741 14.4429L12.741 19.7762C12.1283 20.1847 11.3005 20.0191 10.892 19.4064C10.5127 18.8375 10.6284 18.083 11.1374 17.6511L11.2618 17.5574L19.2618 12.2241ZM18.6681 20.0001C18.3125 20.0001 17.9747 19.8569 17.7259 19.6064C17.6014 19.4811 17.5125 19.32 17.4414 19.1589C17.3703 18.9979 17.3347 18.8386 17.3347 18.6578C17.3347 18.4789 17.3703 18.2999 17.4414 18.1388C17.5125 17.9777 17.6014 17.8346 17.7259 17.7093C18.2236 17.2082 19.1125 17.2082 19.6103 17.7093C19.7347 17.8346 19.8414 17.9777 19.9125 18.1388C19.9659 18.2999 20.0014 18.4789 20.0014 18.6578C20.0014 18.8386 19.9659 18.9979 19.9125 19.1589C19.8414 19.32 19.7347 19.4811 19.6103 19.6064C19.3614 19.8569 19.0236 20.0001 18.6681 20.0001ZM12.837 14.5577C12.997 14.6292 13.157 14.6668 13.3348 14.6668C13.5126 14.6668 13.6903 14.6292 13.8503 14.5577C14.0104 14.4861 14.1526 14.3985 14.277 14.2732C14.4014 14.1301 14.5081 13.987 14.5792 13.826C14.6503 13.6632 14.6681 13.504 14.6681 13.3251C14.6681 12.9674 14.5258 12.6257 14.277 12.3771C13.7792 11.8744 12.9081 11.8744 12.3925 12.3771C12.2681 12.4844 12.1792 12.6454 12.1081 12.8064C12.037 12.9674 12.0014 13.1445 12.0014 13.3251C12.0014 13.504 12.037 13.6632 12.1081 13.826C12.1792 13.987 12.2681 14.1301 12.3925 14.2732C12.517 14.3985 12.677 14.4861 12.837 14.5577Z" fill="#EA5A43"/>
                                </g>
                                </g>
                            </svg>
                        </span>
                        Special Limited Offer Get
                        <b> 30 </b>% off Now.
                        <a href="#" class="see-more viewMore">
                            get the offer
                            <i class="far fa-angle-right"></i>
                        </a>
                    </div>
                    <div class="offerBar--countdown">Ends in 10:53:43</div>
                </div>
               <!-- breadcrumbs -->
                <nav class="breadCrumb wow fadeInDown" data-wow-duration="1s">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item"><a href="#">Digital Orthodontic Solutions</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cephalometric Analysis</li>
                    </ol>
                </nav>
                <!-- breadcrumbs -->
            </div>
        </div>
        <div class="container">
            <div class="productDetailWrapper">
                <div class="productDetail__left">
                    <div class="productDetail__left--item wow fadeInDown" data-wow-duration="1s">
                         <div class="swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="productDetail__left--item--image">
                                         <img
                                            src="src/images/productDetail/product-image1.png"
                                            alt=""
                                            class="img-fluid"
                                        />
                                    </div> 
                                </div>
                                <div class="swiper-slide">
                                    <div class="productDetail__left--item--image">
                                         <img
                                            src="src/images/productDetail/product-image2.png"
                                            alt=""
                                            class="img-fluid"
                                        />
                                    </div> 
                                </div>
                                <div class="swiper-slide">
                                    <div class="productDetail__left--item--image">
                                         <img
                                            src="src/images/productDetail/product-image1.png"
                                            alt=""
                                            class="img-fluid"
                                        />
                                    </div> 
                                </div>
                            </div>
                            <!-- pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="productDetail__left--item--image">
                            <img
                                src="src/images/productDetail/product-image1.png"
                                alt=""
                                class="img-fluid"
                            />
                        </div>
                        <h5 class="wow fadeInUp" data-wow-duration="1s">Description</h5>
                        <p class="wow fadeInUp" data-wow-duration="1s">
                            Guide Package Includes: 3D implant treatment planning -
                            3D surgical guide design - 3D printing for surgical
                            guide - Sleeves This computed custom-made device will
                            take your implantology practice to a new era! .It
                            carries all implant placement plan information to the
                            oral cavity and makes sure that implants are placed
                            exactly in the same position, length, diameter and
                            angulation.
                        </p>
                        <a href="#" class="wow fadeInUp" data-wow-duration="1s">View guide design sample</a>
                    </div>
                    <div class="productDetail__left--item wow fadeInDown" data-wow-duration="1s">
                        <div class="productDetail__left--item--image">
                            <img
                                src="src/images/productDetail/product-image1.png"
                                alt=""
                                class="img-fluid"
                            />
                        </div>
                        <h5 class="wow fadeInUp" data-wow-duration="1s">What is Computer Guided Implant Placement?</h5>
                        <p class="wow fadeInUp" data-wow-duration="1s">
                            Using computer to
                            <span>simulate implant placement,</span> then on the
                            basis of this simulation design and fabricate a device
                            called (surgical guide) which is then used during
                            surgery to place implants in the same positions,
                            dimensions, and angulation previously planned.
                        </p>
                        <p class="wow fadeInUp" data-wow-duration="1s">
                            The conventional method of using the patient’s panoramic
                            x-ray to plan implant surgery cannot transfer the
                            planning exactly as planned. Moreover, it cannot be
                            valid for bone density and measurements especially for
                            buccolingual dimensions.
                        </p>
                    </div>
                    <div class="productDetail__left--item">
                        <h5 class="wow fadeInUp" data-wow-duration="1s">
                            Why I need to use surgical guide for implant placement?
                        </h5>
                        <p class="wow fadeInUp" data-wow-duration="1s">
                            The main attribute of using surgical guide in placing
                            implants is Accuracy. In many times, what happens during
                            positioning of implants in patient mouth is different
                            than the plan. This shortage in accuracy results in
                            biological, mechanical, or esthetical problems or even
                            failure. Surgical guide is a device that is fabricated
                            specially for this mission; eliminate the gap between
                            the expectations and reality (the plan and the result)
                        </p>
                        <div class="productDetail__left--item--image wow fadeInDown" data-wow-duration="1s">
                            <img
                                src="src/images/productDetail/product-image2.png"
                                alt=""
                                class="img-fluid"
                            />
                        </div>
                        <div class="productDetail__left--item--image wow fadeInDown" data-wow-duration="1s">
                            <img
                                src="src/images/productDetail/product-image3.png"
                                alt=""
                                class="img-fluid"
                            />
                        </div>
                        <p class="wow fadeInUp" data-wow-duration="1s">
                            Computer guided implant placement is simply to do the
                            operation first on computer, the fabricate a device (
                            surgical guide) that transfers the plan to patient mouth
                            and makes sure that implants will be placed exactly as
                            planned before on computer.
                        </p>
                    </div>
                    <div class="productDetail__left--item">
                        <p class="bold-text wow fadeInUp" data-wow-duration="1s">
                            The following infographic shows also other many
                            advantages and attributes of using surgical guide:
                        </p>
                        <div class="productDetail__left--item--image wow fadeInDown" data-wow-duration="1s">
                            <img
                                src="src/images/productDetail/product-image4.png"
                                alt=""
                                class="img-fluid"
                            />
                        </div>
                        <h5 class="wow fadeInUp" data-wow-duration="1s">
                            What are the main requirements for surgical guide
                            fabrication?
                        </h5>
                        <p class="wow fadeInUp" data-wow-duration="1s">
                            CBCT (cone beam computed tomogram)<br />
                            In many cases : patient digital impression or model
                            scan<br />
                            Guided surgery kit ( universal or provided by your
                            implant system provider)
                        </p>
                    </div>
                </div>
                <div class="productDetail__right wow fadeInRight" data-wow-duration="1s">
                    <div class="productDetail__right--selectItems">
                        <label class="bestSeller">best seller</label>
                        <div class="productDetail__right--selectItems--subTitle">
                            PART NUMBER: sg3
                        </div>
                        <div class="productDetail__right--selectItems--Title">
                            Surgical Guide for implant (More than 3 implants)
                        </div>
                        <div class="productDetail__right--selectItems--price">
                            $445.00<span>$810.00</span>
                        </div>
                        <p class="productDetail__right--selectItems--soldBy">
                            Sold by Seller 2
                        </p>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="radio" />
                                <span
                                    ><b>3D Printing</b> (Cost = <i>$75.00</i>
                                    <i class="oldPrice">$130.00)</i></span
                                >
                            </label>
                        </div>
                        <div class="productDetail__right--selectItems--quantity">
                            <label>Quantity</label>
                            <div class="form-group">
                                <input class='form-control' type="text" value="1" name="touchSpin">
                            </div>
                        </div>
                        <div class="button-row">
                            <a href="#" class="btn btn-default full"
                                >Start This Service</a
                            >
                        </div>
                        <a href="#" class="see-more viewMore"
                            >Request More Info
                            <i class="far fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="productDetail__relatedProducts">
                <div class="sectionTitle">Related Products</div>
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="productItem card">
                                    <div class="card__body">
                                        <div class="productItem__image">
                                            <img
                                                alt=""
                                                src="src/images/product-image.png"
                                                class="img-fluid"
                                            />
                                        </div>
                                        <div class="productItem__info">
                                            <div class="productItem__info--title">
                                                DDS-Pro software for guide design (full version)
                                            </div>
                                            <div class="productItem__info--price">
                                                $79.00
                                                <span class="productItem__info--oldprice">
                                                    $109.00
                                                </span>
                                            </div>
                                            <p>Our DSD team will create 2D and 3D digital smile design project for your</p>
                                            <a href="#" class="productItem__info--seemore"
                                                >see more details</a
                                            >
                                            <div class="button-row">
                                                <a href="#" class="btn btn-default full">
                                                    <span class="icon big">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M2.10579 1.55301C2.33514 1.09431 2.86945 0.888905 3.34011 1.05965L3.44743 1.10579L3.59733 1.18074C4.97532 1.86974 5.95455 3.15299 6.26045 4.65386L6.30312 4.89259L6.31822 4.99522L18.5608 4.99592C18.6925 4.99592 18.8239 5.00458 18.9543 5.02183L19.1489 5.05413C20.7156 5.36733 21.7514 6.84803 21.5322 8.41017L21.5026 8.58402L20.3023 14.5883C20.0336 15.9323 18.8916 16.9154 17.5381 16.995L17.3605 17.0002H8.71606C7.29457 17.0002 6.07719 16.0046 5.78287 14.6301L5.75094 14.4564L4.32637 5.1967C4.18828 4.29909 3.65187 3.51603 2.87337 3.06181L2.7029 2.96959L2.55301 2.89465C2.05903 2.64766 1.8588 2.04698 2.10579 1.55301ZM7.72768 14.1523C7.79696 14.6026 8.15995 14.9441 8.60359 14.9939L8.71606 15.0002H17.3605C17.7975 15.0002 18.1788 14.7175 18.3112 14.3103L18.3411 14.1963L19.5414 8.19195C19.6497 7.65038 19.2984 7.12359 18.7569 7.01532L18.6593 7.00078L18.5608 6.99592L6.62622 6.99522L7.72768 14.1523Z" fill="currentColor"/>
                                                            <g opacity="0.5">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 9C11.4477 9 11 9.44772 11 10C11 10.5523 11.4477 11 12 11H16C16.5523 11 17 10.5523 17 10C17 9.44772 16.5523 9 16 9H12ZM9.5 19C10.3275 19 11 19.673 11 20.501C11 21.329 10.3275 22 9.5 22C8.67255 22 8 21.329 8 20.501C8 19.673 8.67255 19 9.5 19ZM16.5 19C17.3275 19 18 19.673 18 20.501C18 21.329 17.3275 22 16.5 22C15.6725 22 15 21.329 15 20.501C15 19.673 15.6725 19 16.5 19Z" fill="currentColor"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Add to Cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="productItem card">
                                    <div class="card__body">
                                        <div class="productItem__image">
                                            <img
                                                alt=""
                                                src="src/images/product-image.png"
                                                class="img-fluid"
                                            />
                                        </div>
                                        <div class="productItem__info">
                                            <div class="productItem__info--title">
                                                DDS-Pro software for guide design (full version)
                                            </div>
                                            <div class="productItem__info--price">
                                                $79.00
                                                <span class="productItem__info--oldprice">
                                                    $109.00
                                                </span>
                                            </div>
                                            <p>Our DSD team will create 2D and 3D digital smile design project for your</p>
                                            <a href="#" class="productItem__info--seemore"
                                                >see more details</a
                                            >
                                            <div class="inline-button">
                                                <a href="#" class="btn btn-default btn-small">
                                                    <span class="icon big only-icon">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M2.10579 1.55301C2.33514 1.09431 2.86945 0.888905 3.34011 1.05965L3.44743 1.10579L3.59733 1.18074C4.97532 1.86974 5.95455 3.15299 6.26045 4.65386L6.30312 4.89259L6.31822 4.99522L18.5608 4.99592C18.6925 4.99592 18.8239 5.00458 18.9543 5.02183L19.1489 5.05413C20.7156 5.36733 21.7514 6.84803 21.5322 8.41017L21.5026 8.58402L20.3023 14.5883C20.0336 15.9323 18.8916 16.9154 17.5381 16.995L17.3605 17.0002H8.71606C7.29457 17.0002 6.07719 16.0046 5.78287 14.6301L5.75094 14.4564L4.32637 5.1967C4.18828 4.29909 3.65187 3.51603 2.87337 3.06181L2.7029 2.96959L2.55301 2.89465C2.05903 2.64766 1.8588 2.04698 2.10579 1.55301ZM7.72768 14.1523C7.79696 14.6026 8.15995 14.9441 8.60359 14.9939L8.71606 15.0002H17.3605C17.7975 15.0002 18.1788 14.7175 18.3112 14.3103L18.3411 14.1963L19.5414 8.19195C19.6497 7.65038 19.2984 7.12359 18.7569 7.01532L18.6593 7.00078L18.5608 6.99592L6.62622 6.99522L7.72768 14.1523Z" fill="currentColor"/>
                                                            <g opacity="0.5">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 9C11.4477 9 11 9.44772 11 10C11 10.5523 11.4477 11 12 11H16C16.5523 11 17 10.5523 17 10C17 9.44772 16.5523 9 16 9H12ZM9.5 19C10.3275 19 11 19.673 11 20.501C11 21.329 10.3275 22 9.5 22C8.67255 22 8 21.329 8 20.501C8 19.673 8.67255 19 9.5 19ZM16.5 19C17.3275 19 18 19.673 18 20.501C18 21.329 17.3275 22 16.5 22C15.6725 22 15 21.329 15 20.501C15 19.673 15.6725 19 16.5 19Z" fill="currentColor"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </a>
                                                <a href="#" class="btn btn-secondary full">
                                                    Start This Service
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="productItem card">
                                    <div class="card__body">
                                        <div class="productItem__image">
                                            <img
                                                alt=""
                                                src="src/images/product-image.png"
                                                class="img-fluid"
                                            />
                                        </div>
                                        <div class="productItem__info">
                                            <div class="productItem__info--title">
                                                DDS-Pro software for guide design (full version)
                                            </div>
                                            <div class="productItem__info--price">
                                                $79.00
                                                <span class="productItem__info--oldprice">
                                                    $109.00
                                                </span>
                                            </div>
                                            <p>Our DSD team will create 2D and 3D digital smile design project for your</p>
                                            <a href="#" class="productItem__info--seemore"
                                                >see more details</a
                                            >
                                            <div class="inline-button">
                                                <a href="#" class="btn btn-default btn-small">
                                                    <span class="icon big only-icon">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M2.10579 1.55301C2.33514 1.09431 2.86945 0.888905 3.34011 1.05965L3.44743 1.10579L3.59733 1.18074C4.97532 1.86974 5.95455 3.15299 6.26045 4.65386L6.30312 4.89259L6.31822 4.99522L18.5608 4.99592C18.6925 4.99592 18.8239 5.00458 18.9543 5.02183L19.1489 5.05413C20.7156 5.36733 21.7514 6.84803 21.5322 8.41017L21.5026 8.58402L20.3023 14.5883C20.0336 15.9323 18.8916 16.9154 17.5381 16.995L17.3605 17.0002H8.71606C7.29457 17.0002 6.07719 16.0046 5.78287 14.6301L5.75094 14.4564L4.32637 5.1967C4.18828 4.29909 3.65187 3.51603 2.87337 3.06181L2.7029 2.96959L2.55301 2.89465C2.05903 2.64766 1.8588 2.04698 2.10579 1.55301ZM7.72768 14.1523C7.79696 14.6026 8.15995 14.9441 8.60359 14.9939L8.71606 15.0002H17.3605C17.7975 15.0002 18.1788 14.7175 18.3112 14.3103L18.3411 14.1963L19.5414 8.19195C19.6497 7.65038 19.2984 7.12359 18.7569 7.01532L18.6593 7.00078L18.5608 6.99592L6.62622 6.99522L7.72768 14.1523Z" fill="currentColor"/>
                                                            <g opacity="0.5">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 9C11.4477 9 11 9.44772 11 10C11 10.5523 11.4477 11 12 11H16C16.5523 11 17 10.5523 17 10C17 9.44772 16.5523 9 16 9H12ZM9.5 19C10.3275 19 11 19.673 11 20.501C11 21.329 10.3275 22 9.5 22C8.67255 22 8 21.329 8 20.501C8 19.673 8.67255 19 9.5 19ZM16.5 19C17.3275 19 18 19.673 18 20.501C18 21.329 17.3275 22 16.5 22C15.6725 22 15 21.329 15 20.501C15 19.673 15.6725 19 16.5 19Z" fill="currentColor"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </a>
                                                <a href="#" class="btn btn-secondary full">
                                                    Start This Service
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="productItem card">
                                    <div class="card__body">
                                        <div class="productItem__image">
                                            <img
                                                alt=""
                                                src="src/images/product-image.png"
                                                class="img-fluid"
                                            />
                                        </div>
                                        <div class="productItem__info">
                                            <div class="productItem__info--title">
                                                DDS-Pro software for guide design (full version)
                                            </div>
                                            <div class="productItem__info--price">
                                                $79.00
                                                <span class="productItem__info--oldprice">
                                                    $109.00
                                                </span>
                                            </div>
                                            <p>Our DSD team will create 2D and 3D digital smile design project for your</p>
                                            <a href="#" class="productItem__info--seemore"
                                                >see more details</a
                                            >
                                            <div class="button-row">
                                                <a href="#" class="btn btn-default full">
                                                    <span class="icon big">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M2.10579 1.55301C2.33514 1.09431 2.86945 0.888905 3.34011 1.05965L3.44743 1.10579L3.59733 1.18074C4.97532 1.86974 5.95455 3.15299 6.26045 4.65386L6.30312 4.89259L6.31822 4.99522L18.5608 4.99592C18.6925 4.99592 18.8239 5.00458 18.9543 5.02183L19.1489 5.05413C20.7156 5.36733 21.7514 6.84803 21.5322 8.41017L21.5026 8.58402L20.3023 14.5883C20.0336 15.9323 18.8916 16.9154 17.5381 16.995L17.3605 17.0002H8.71606C7.29457 17.0002 6.07719 16.0046 5.78287 14.6301L5.75094 14.4564L4.32637 5.1967C4.18828 4.29909 3.65187 3.51603 2.87337 3.06181L2.7029 2.96959L2.55301 2.89465C2.05903 2.64766 1.8588 2.04698 2.10579 1.55301ZM7.72768 14.1523C7.79696 14.6026 8.15995 14.9441 8.60359 14.9939L8.71606 15.0002H17.3605C17.7975 15.0002 18.1788 14.7175 18.3112 14.3103L18.3411 14.1963L19.5414 8.19195C19.6497 7.65038 19.2984 7.12359 18.7569 7.01532L18.6593 7.00078L18.5608 6.99592L6.62622 6.99522L7.72768 14.1523Z" fill="currentColor"/>
                                                            <g opacity="0.5">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 9C11.4477 9 11 9.44772 11 10C11 10.5523 11.4477 11 12 11H16C16.5523 11 17 10.5523 17 10C17 9.44772 16.5523 9 16 9H12ZM9.5 19C10.3275 19 11 19.673 11 20.501C11 21.329 10.3275 22 9.5 22C8.67255 22 8 21.329 8 20.501C8 19.673 8.67255 19 9.5 19ZM16.5 19C17.3275 19 18 19.673 18 20.501C18 21.329 17.3275 22 16.5 22C15.6725 22 15 21.329 15 20.501C15 19.673 15.6725 19 16.5 19Z" fill="currentColor"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Add to Cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="productItem card">
                                    <div class="card__body">
                                        <div class="productItem__image">
                                            <img
                                                alt=""
                                                src="src/images/product-image.png"
                                                class="img-fluid"
                                            />
                                        </div>
                                        <div class="productItem__info">
                                            <div class="productItem__info--title">
                                                DDS-Pro software for guide design (full version)
                                            </div>
                                            <div class="productItem__info--price">
                                                $79.00
                                                <span class="productItem__info--oldprice">
                                                    $109.00
                                                </span>
                                            </div>
                                            <p>Our DSD team will create 2D and 3D digital smile design project for your</p>
                                            <a href="#" class="productItem__info--seemore"
                                                >see more details</a
                                            >
                                            <div class="button-row">
                                                <a href="#" class="btn btn-default full">
                                                    <span class="icon big">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M2.10579 1.55301C2.33514 1.09431 2.86945 0.888905 3.34011 1.05965L3.44743 1.10579L3.59733 1.18074C4.97532 1.86974 5.95455 3.15299 6.26045 4.65386L6.30312 4.89259L6.31822 4.99522L18.5608 4.99592C18.6925 4.99592 18.8239 5.00458 18.9543 5.02183L19.1489 5.05413C20.7156 5.36733 21.7514 6.84803 21.5322 8.41017L21.5026 8.58402L20.3023 14.5883C20.0336 15.9323 18.8916 16.9154 17.5381 16.995L17.3605 17.0002H8.71606C7.29457 17.0002 6.07719 16.0046 5.78287 14.6301L5.75094 14.4564L4.32637 5.1967C4.18828 4.29909 3.65187 3.51603 2.87337 3.06181L2.7029 2.96959L2.55301 2.89465C2.05903 2.64766 1.8588 2.04698 2.10579 1.55301ZM7.72768 14.1523C7.79696 14.6026 8.15995 14.9441 8.60359 14.9939L8.71606 15.0002H17.3605C17.7975 15.0002 18.1788 14.7175 18.3112 14.3103L18.3411 14.1963L19.5414 8.19195C19.6497 7.65038 19.2984 7.12359 18.7569 7.01532L18.6593 7.00078L18.5608 6.99592L6.62622 6.99522L7.72768 14.1523Z" fill="currentColor"/>
                                                            <g opacity="0.5">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 9C11.4477 9 11 9.44772 11 10C11 10.5523 11.4477 11 12 11H16C16.5523 11 17 10.5523 17 10C17 9.44772 16.5523 9 16 9H12ZM9.5 19C10.3275 19 11 19.673 11 20.501C11 21.329 10.3275 22 9.5 22C8.67255 22 8 21.329 8 20.501C8 19.673 8.67255 19 9.5 19ZM16.5 19C17.3275 19 18 19.673 18 20.501C18 21.329 17.3275 22 16.5 22C15.6725 22 15 21.329 15 20.501C15 19.673 15.6725 19 16.5 19Z" fill="currentColor"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    Add to Cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div  class="sliderNavigation">
                        <div  class="container">
                            <div class="sliderNavigation__controls">
                                <button class="sliderNavigation__controls--arrow prevArrow">
                                    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.60948 0.722556C8.09012 1.2032 8.12709 1.95951 7.72039 2.48256L7.60948 2.60817L3.22 6.9987L7.60948 11.3892C8.09012 11.8699 8.12709 12.6262 7.72039 13.1492L7.60948 13.2748C7.12883 13.7555 6.37252 13.7925 5.84947 13.3858L5.72386 13.2748L0.390525 7.94151C-0.0901206 7.46086 -0.127094 6.70455 0.279606 6.1815L0.390525 6.05589L5.72386 0.722555C6.24456 0.201856 7.08878 0.201856 7.60948 0.722556Z" fill="currentColor"/>
                                    </svg>
                                </button>
                                <button class="sliderNavigation__controls--arrow nextArrow">
                                    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.390524 13.2774C-0.090121 12.7968 -0.127094 12.0405 0.279606 11.5174L0.390524 11.3918L4.78 7.0013L0.390524 2.61078C-0.0901215 2.13013 -0.127094 1.37383 0.279606 0.85077L0.390524 0.725161C0.871169 0.244515 1.62748 0.207542 2.15053 0.614242L2.27614 0.72516L7.60947 6.05849C8.09012 6.53914 8.12709 7.29545 7.72039 7.8185L7.60947 7.94411L2.27614 13.2774C1.75544 13.7981 0.911223 13.7981 0.390524 13.2774Z" fill="currentColor"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="newsLetter">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="d-flex align-items-center">
                            <div class="newsLetter__icon">
                                <svg width="114" height="102" viewBox="0 0 114 102" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.2" d="M113.509 54.3999C113.509 28.1111 92.1975 6.79982 65.9087 6.79983C39.6199 6.79983 18.3086 28.1111 18.3086 54.3999C18.3086 80.6887 39.6199 102 65.9087 102C92.1975 102 113.509 80.6887 113.509 54.3999Z" fill="#ABABAB"/>
                                    <path d="M94.2324 35.7344H38.582V71.0341H94.2324V35.7344Z" fill="white"/>
                                    <path d="M94.2308 35.7344C91.985 37.8881 89.6931 39.9496 87.4242 42.0687C85.1553 44.1879 82.8174 46.2034 80.514 48.2534C78.2106 50.3034 75.7114 52.5032 73.3043 54.6108L73.0855 54.8181L66.6705 60.3809L66.3595 60.6342L66.0486 60.3809L59.6451 54.8066L59.4147 54.6108C56.9962 52.4994 54.5891 50.3764 52.1936 48.2419C49.8901 46.2034 47.5867 44.1303 45.2833 42.0687C42.9799 40.0072 40.7341 37.8881 38.4883 35.7344C40.8723 37.5771 43.2218 39.512 45.5943 41.4008C47.9668 43.2896 50.3048 45.2475 52.6427 47.1593C55.2456 49.3245 57.8523 51.5166 60.4628 53.7355L66.3019 58.6994C68.2483 57.0409 70.1947 55.3709 72.1411 53.7355C74.7439 51.5243 77.3506 49.3322 79.9612 47.1593C82.2646 45.2167 84.6141 43.2972 87.0096 41.4008C89.4973 39.512 91.8467 37.5771 94.2308 35.7344Z" fill="#DBDBDB"/>
                                    <path d="M60.6353 54.5879H60.5662L59.6909 55.325C58.2973 56.4767 56.8692 57.6284 55.4065 58.7801C53.6329 60.1276 51.8247 61.429 50.005 62.7074C48.1854 63.9858 46.3311 65.2296 44.4538 66.4274C42.5766 67.6252 40.6762 68.8114 38.6953 69.8825C40.3883 68.3968 42.1504 67.0263 43.9125 65.6788C45.6746 64.3313 47.5059 63.0299 49.3255 61.74C51.1452 60.4501 52.9995 59.2177 54.8768 58.0085C56.374 57.0525 57.8942 56.1081 59.4836 55.2213L60.5777 54.6109L60.6353 54.5879Z" fill="#DBDBDB"/>
                                    <path d="M94.0923 69.9058C92.1344 68.8232 90.2226 67.6023 88.3338 66.4506C86.445 65.2989 84.6138 63.9975 82.771 62.7306C80.9283 61.4638 79.1432 60.1508 77.3696 58.8033C75.9184 57.6516 74.4903 56.5805 73.1082 55.3482L72.2214 54.5996H72.1523H72.2329L73.3271 55.2215C74.8819 56.1083 76.3906 57.0527 77.9339 58.0087C79.8112 59.1604 81.6424 60.4733 83.4736 61.7402C85.3048 63.007 87.113 64.32 88.8866 65.679C90.6602 67.038 92.4108 68.4201 94.0923 69.9058Z" fill="#DBDBDB"/>
                                    <path d="M92.4293 43.6264C96.7801 41.2552 98.3849 35.806 96.0138 31.4552C93.6426 27.1044 88.1934 25.4996 83.8426 27.8707C79.4918 30.2419 77.887 35.6911 80.2582 40.0419C82.6293 44.3927 88.0785 45.9975 92.4293 43.6264Z" fill="#1A90D2"/>
                                    <path d="M87.3144 36.5077L87.3144 32.0972C87.2994 31.9589 87.3137 31.819 87.3563 31.6867C87.399 31.5543 87.469 31.4324 87.5619 31.3289C87.6548 31.2254 87.7685 31.1427 87.8955 31.086C88.0225 31.0293 88.16 31.0001 88.299 31.0001C88.4381 31.0001 88.5756 31.0293 88.7026 31.086C88.8296 31.1427 88.9433 31.2254 89.0361 31.3289C89.129 31.4324 89.1991 31.5543 89.2418 31.6867C89.2844 31.819 89.2987 31.9589 89.2838 32.0972L89.2838 36.5077C89.2987 36.646 89.2844 36.7859 89.2418 36.9182C89.1991 37.0506 89.129 37.1725 89.0362 37.276C88.9433 37.3795 88.8296 37.4623 88.7026 37.5189C88.5756 37.5756 88.4381 37.6049 88.299 37.6049C88.16 37.6049 88.0225 37.5756 87.8955 37.5189C87.7685 37.4623 87.6548 37.3795 87.5619 37.276C87.4691 37.1725 87.399 37.0506 87.3563 36.9182C87.3137 36.7859 87.2994 36.646 87.3144 36.5077ZM88.3048 40.8612C88.1738 40.8612 88.044 40.8352 87.9231 40.7847C87.8022 40.7342 87.6925 40.6602 87.6004 40.567C87.5082 40.4738 87.4355 40.3633 87.3864 40.2418C87.3373 40.1203 87.3128 39.9902 87.3144 39.8592V39.5022C87.2994 39.3639 87.3137 39.2241 87.3563 39.0917C87.399 38.9593 87.4691 38.8374 87.5619 38.7339C87.6548 38.6305 87.7685 38.5477 87.8955 38.491C88.0225 38.4344 88.16 38.4051 88.299 38.4051C88.4381 38.4051 88.5756 38.4344 88.7026 38.491C88.8296 38.5477 88.9433 38.6305 89.0362 38.7339C89.129 38.8374 89.1991 38.9593 89.2418 39.0917C89.2844 39.2241 89.2987 39.3639 89.2838 39.5022V39.8592C89.287 39.9927 89.2629 40.1253 89.2129 40.2491C89.163 40.3729 89.0883 40.4852 88.9933 40.579C88.8984 40.6729 88.7853 40.7463 88.661 40.7948C88.5366 40.8434 88.4037 40.8659 88.2703 40.8612H88.3048Z" fill="white"/>
                                    <path d="M48.7436 7.28047L45.4141 17.1429L52.7883 11.1108L81.3069 0L48.7436 7.28047Z" fill="#888888"/>
                                    <path d="M53.9855 13.4508L45.4141 17.1429L52.807 11L53.9855 13.4508Z" fill="#888888"/>
                                    <path d="M52.8086 11L81.3086 0L58.4064 23.5714L52.8086 11Z" fill="#A7A7A7"/>
                                    <path d="M48.5541 7.5L36.3086 0L81.3086 0.0694953L48.5541 7.5Z" fill="#A7A7A7"/>
                                    <path opacity="0.5" d="M48.8088 15C2.30878 34 -9.71088 76.4201 10.3088 85C16.9112 87.8296 21.6175 87.7317 29.3088 85" stroke="#888888" stroke-linecap="round" stroke-dasharray="4 4"/>
                                </svg>

                            </div>
                            <div>
                                <h4>Subscribe to Newsletter</h4>
                                <p>Stay updated with all the latest news and offers</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <input type="email"  name="lname" class="form-control" placeholder="Type your email">
                                <button type="submit" class="btn btn-default">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include_once "includes/footer.php";?>

