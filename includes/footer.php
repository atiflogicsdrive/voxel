    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xl-2">
                    <div class="footer__logo">
                        <a href="#">
                            <img src="src/images/logo.png" alt="footerLgo" class="img-fluid hide-on-md">
                            <img
                                    src="src/images/Logo-mob.svg"
                                    class="img-fluid d-none show-on-md"
                                    alt="vetro logo"
                                />
                        </a>
                        <p class="poweredBy">Powered By Photon Group</p>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="footer__links">
                        <div>
                            <h5>Product</h5>
                            <ul>
                                <li><a href="#">Shop</a></li>
                                <li><a href="#">Pricing</a></li>
                            </ul>
                        </div>
                        <div>
                            <h5>Be Partner</h5>
                            <ul>
                                <li><a href="#">Implant Companies</a></li>
                                <li><a href="#">Radiology Centres</a></li>
                            </ul>
                        </div>
                        <div>
                            <h5>login</h5>
                            <ul>
                                <li><a href="#">Track your case now</a></li>
                                <li><a href="#">Claim your first order</a></li>
                            </ul>
                        </div>
                        <div>
                            <h5>Get Inspired</h5>
                            <ul>
                                <li><a href="#">Edu-videos</a></li>
                            </ul>
                        </div>
                        <div>
                            <h5>Contacts</h5>
                            <ul class="contacts">
                                <li class="phone"><a href="tel:+2 0111 1163 354">+44 7592090094</a></li>
                                <li class="email"><a href="mailto:info@voxel-online.com">info@voxel3di.co.uk</a></li>
                                <li class="location">22 Wenlock Road |London |UK</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="footer__social">
                        <h5>Get in touch</h5>
                        <ul>
                            <li>
                                <a href="#" target="_blank">
                                    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.00239 13.7165V7.58735H6.98383L7.28047 5.19839H5.00239V3.67343C5.00239 2.98175 5.18719 2.51039 6.14239 2.51039H7.36063V0.375349C6.77092 0.309876 6.17798 0.277824 5.58464 0.279349C3.8283 0.279349 2.62591 1.39247 2.62591 3.43679V5.19839H0.640625V7.58735H2.62687V13.7169L5.00239 13.7165Z" fill="currentColor"/>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <mask id="mask0_3628:26394" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="14" height="14">
                                        <rect x="0.28125" y="0.279297" width="13.44" height="13.44" fill="white"/>
                                        </mask>
                                        <g mask="url(#mask0_3628:26394)">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6954 2.83867C13.1914 3.05971 12.6577 3.20528 12.1114 3.27068C12.6861 2.92533 13.1164 2.38366 13.3229 1.74571C12.7798 2.06183 12.1881 2.28582 11.5718 2.4086C10.7989 1.585 9.60144 1.3168 8.55103 1.73201C7.50062 2.14722 6.81018 3.16166 6.80926 4.29115C6.80926 4.50327 6.83312 4.71436 6.88038 4.92078C4.66481 4.81475 2.59905 3.77085 1.19949 2.05004C0.952458 2.46986 0.823587 2.94869 0.826523 3.43579C0.825681 4.35752 1.2856 5.21853 2.05207 5.73027C1.61447 5.71633 1.18654 5.59797 0.803963 5.38508L0.803963 5.41915C0.803178 6.73104 1.72673 7.86161 3.01218 8.12256C2.60803 8.23161 2.18446 8.248 1.77309 8.17052C2.1361 9.29309 3.17157 10.0615 4.35117 10.0838C3.37649 10.8487 2.17303 11.2639 0.934043 11.2627C0.715908 11.2622 0.49797 11.2496 0.28125 11.2248C1.5446 12.0328 3.01326 12.4615 4.51294 12.4598C9.58319 12.4598 12.3523 8.2622 12.3523 4.62764C12.3523 4.51051 12.3523 4.39244 12.3442 4.27484C12.8848 3.88603 13.3512 3.40333 13.7213 2.84972L13.6954 2.83867Z" fill="currentColor"/>
                                        </g>
                                    </svg>

                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <svg width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.4333 1.66467C13.3555 1.38314 13.2068 1.1263 13.0013 0.918755C12.797 0.712076 12.5427 0.561907 12.2631 0.482915C11.2157 0.199235 7.00082 0.199235 7.00082 0.199235C7.00082 0.199235 2.79794 0.192035 1.74003 0.483395C1.4604 0.562387 1.20603 0.712556 1.00179 0.919234C0.796291 1.12678 0.647539 1.38363 0.569787 1.66515C0.37124 2.74883 0.273383 3.84856 0.277466 4.95027C0.274484 6.04743 0.372643 7.14278 0.570699 8.22218C0.648424 8.5038 0.797195 8.76074 1.00275 8.96835C1.20699 9.17503 1.46136 9.3252 1.74099 9.40419C2.7869 9.68835 7.00322 9.68835 7.00322 9.68835C7.00322 9.68835 11.2066 9.68835 12.2655 9.40419C12.5451 9.3252 12.7994 9.17503 13.0037 8.96835C13.2092 8.76081 13.3579 8.50396 13.4357 8.22243C13.6293 7.14246 13.7229 6.04695 13.7155 4.94979C13.7229 3.84853 13.6285 2.74881 13.4333 1.66481L13.4333 1.66467ZM5.65698 6.98158V2.91118L9.16482 4.94974L5.65698 6.98158Z" fill="currentColor"/>
                                </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <p>© 2021 VOXEL. All rights reserved.</p>
                <ul>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms & Condition</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
</body>
</html>