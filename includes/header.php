<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
    <script src="src/js/jquery.mCustomScrollbar.js"></script>
    <script src="src/js/jquery.bootstrap-touchspin.js"></script>
    <script src="src/js/wow.js"></script>
    <script src="src/js/custom.js"></script> 
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- swiper slider -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/7.0.1/swiper-bundle.css" integrity="sha512-Fp08V+RWsgLTrE/mydkeVu2Riu+nG0I1zahQHo/7On0kiYWakR0B5ErE9nqFI08YzZTSNoEL3nDrXjVQNyNHxw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="src/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="src/css/jquery.bootstrap-touchspin.css">
    <link rel="stylesheet" href="src/css/animate.css">
    <link href="src/css/main.css" rel="stylesheet"/>

    <title>voxel-html</title>
</head>
<body <?php if(isset($ishome)){ echo "class='front-page'";}?> >
    <?php include_once "includes/modal.php";?> 
    <div class="wrapper">
        <header>
			<div class="container">
				<div class="headerWrapper">
                    <div class="flex-row">
                        <div class="left-side">
                            <a href="#" class="menu-icon d-none show-on-lg" id="menu">
                                <img
                                    src="src/images/menu-icon.png"
                                    class="img-fluid"
                                    alt="menu"
                                />
                                <div class="closeIcon">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M7.6129 6.2097C7.22061 5.90468 6.65338 5.93241 6.29289 6.29289C5.90237 6.68342 5.90237 7.31658 6.29289 7.70711L10.5858 12L6.29289 16.2929L6.2097 16.3871C5.90468 16.7794 5.93241 17.3466 6.29289 17.7071C6.68342 18.0976 7.31658 18.0976 7.70711 17.7071L12 13.4142L16.2929 17.7071L16.3871 17.7903C16.7794 18.0953 17.3466 18.0676 17.7071 17.7071C18.0976 17.3166 18.0976 16.6834 17.7071 16.2929L13.4142 12L17.7071 7.70711L17.7903 7.6129C18.0953 7.22061 18.0676 6.65338 17.7071 6.29289C17.3166 5.90237 16.6834 5.90237 16.2929 6.29289L12 10.5858L7.70711 6.29289L7.6129 6.2097Z" fill="#8E8E8E"/>
                                    </svg>
                                </div>
                            </a>
                            <a href="@index.php" title="vetro" class="logo">
                                <img
                                    src="src/images/logo.png"
                                    class="img-fluid hide-on-lg"
                                    alt="logo"
                                />
                                <img
                                    src="src/images/Logo-mob.svg"
                                    class="img-fluid d-none show-on-lg"
                                    alt="vetro logo"
                                />
                            </a>
                            <nav>
                                <div>
                                    <!-- on mobile -->
                                    <div class="search d-none show-on-lg">
                                        <input type="text" class="form-control" placeholder="Search Everything">
                                    </div>
                                    <!-- on mobile -->
                                    <ul>
                                        <li><a href="shop.php">Shop</a></li>
                                        <li><a href="#">Pricing and Offers</a></li>
                                        <li><a href="about.php">About us</a></li>
                                        <li><a href="#">Be a Partner</a></li>
                                        <li><a href="about.php">Education</a></li>
                                        <li><a href="#">How it Works?</a></li>
                                    </ul>
                                    <!-- on mobile -->
                                    <a href="#" class='btn btn-primary d-none show-on-lg'>Get Started</a>
                                    <!-- on mobile -->
                                </div>
                            </nav>
                        </div>
                        <div class="right-side">
                            <ul class="list-unstyled">
                                <!-- <li class="search hide-on-lg" >
                                    <form>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="What're we looking for ?">
                                            <i class="close-search fas fa-times" aria-hidden="true"></i>
                                        </div>
                                        <a class="search-icon" href="#">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3ZM11 5C14.3137 5 17 7.68629 17 11C17 14.3137 14.3137 17 11 17C7.68629 17 5 14.3137 5 11C5 7.68629 7.68629 5 11 5Z" fill="currentColor"/>
                                                <g opacity="0.5">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M15.2926 15.2971C15.6529 14.9365 16.2201 14.9085 16.6125 15.2133L16.7068 15.2965L20.7068 19.2928C21.0975 19.6832 21.0978 20.3164 20.7074 20.7071C20.3471 21.0677 19.7799 21.0957 19.3875 20.7908L19.2932 20.7077L15.2932 16.7113C14.9025 16.321 14.9022 15.6878 15.2926 15.2971Z" fill="currentColor"/>
                                            </g>
                                        </svg>

                                        </a>
                                    </form>
                                </li> -->
                                <li class="dropdown">
                                    <a href="#" class="cart dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"></a>
                                    <div class="dropdown-menu">
                                        <ul class='inner-scroll'>
                                            <li>
                                                <a class="dropdown-item" href="#">
                                                    <div class="cartItems">
                                                        <div class="cartItems__products">
                                                            <div class="cartItem__product">
                                                                <div class="cartItem__product--image">
                                                                    <img
                                                                        src="src/images/shoppingCart/mobile.png"
                                                                        alt=""
                                                                        class="img-fluid"
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <p class="cartItem__product--title">3D Implant Planning</p>
                                                                    <p class="cartItem__product--price">$400.00<span>$655.00</span></p>
                                                                </div>
                                                                <div class="checkbox hideDesktop">
                                                                    <label>
                                                                        <input type="checkbox" name="radio1" />
                                                                        <span>3D Printing</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <span class="cartItems__products--close">
                                                            <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.66129 5.17475C6.31803 4.92057 5.82171 4.94367 5.50628 5.24408C5.16457 5.56951 5.16457 6.09715 5.50628 6.42259L9.26256 10L5.50628 13.5774L5.43349 13.6559C5.16659 13.9828 5.19086 14.4555 5.50628 14.7559C5.84799 15.0814 6.40201 15.0814 6.74372 14.7559L10.5 11.1785L14.2563 14.7559L14.3387 14.8252C14.682 15.0794 15.1783 15.0563 15.4937 14.7559C15.8354 14.4305 15.8354 13.9028 15.4937 13.5774L11.7374 10L15.4937 6.42259L15.5665 6.34408C15.8334 6.01717 15.8091 5.54448 15.4937 5.24408C15.152 4.91864 14.598 4.91864 14.2563 5.24408L10.5 8.82149L6.74372 5.24408L6.66129 5.17475Z" fill="#C9C9C9"/>
                                                            </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" href="#">
                                                    <div class="cartItems">
                                                        <div class="cartItems__products">
                                                            <div class="cartItem__product">
                                                                <div class="cartItem__product--image">
                                                                    <img
                                                                        src="src/images/shoppingCart/mobile.png"
                                                                        alt=""
                                                                        class="img-fluid"
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <p class="cartItem__product--title">3D Implant Planning</p>
                                                                    <p class="cartItem__product--price">$400.00<span>$655.00</span></p>
                                                                </div>
                                                                <div class="checkbox hideDesktop">
                                                                    <label>
                                                                        <input type="checkbox" name="radio2" />
                                                                        <span>3D Printing</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <span class="cartItems__products--close">
                                                            <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.66129 5.17475C6.31803 4.92057 5.82171 4.94367 5.50628 5.24408C5.16457 5.56951 5.16457 6.09715 5.50628 6.42259L9.26256 10L5.50628 13.5774L5.43349 13.6559C5.16659 13.9828 5.19086 14.4555 5.50628 14.7559C5.84799 15.0814 6.40201 15.0814 6.74372 14.7559L10.5 11.1785L14.2563 14.7559L14.3387 14.8252C14.682 15.0794 15.1783 15.0563 15.4937 14.7559C15.8354 14.4305 15.8354 13.9028 15.4937 13.5774L11.7374 10L15.4937 6.42259L15.5665 6.34408C15.8334 6.01717 15.8091 5.54448 15.4937 5.24408C15.152 4.91864 14.598 4.91864 14.2563 5.24408L10.5 8.82149L6.74372 5.24408L6.66129 5.17475Z" fill="#C9C9C9"/>
                                                            </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" href="#">
                                                    <div class="cartItems">
                                                        <div class="cartItems__products">
                                                            <div class="cartItem__product">
                                                                <div class="cartItem__product--image">
                                                                    <img
                                                                        src="src/images/shoppingCart/mobile.png"
                                                                        alt=""
                                                                        class="img-fluid"
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <p class="cartItem__product--title">3D Implant Planning</p>
                                                                    <p class="cartItem__product--price">$400.00<span>$655.00</span></p>
                                                                </div>
                                                                <div class="checkbox hideDesktop">
                                                                    <label>
                                                                        <input type="checkbox" name="radio3" />
                                                                        <span>3D Printing</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <span class="cartItems__products--close">
                                                            <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.66129 5.17475C6.31803 4.92057 5.82171 4.94367 5.50628 5.24408C5.16457 5.56951 5.16457 6.09715 5.50628 6.42259L9.26256 10L5.50628 13.5774L5.43349 13.6559C5.16659 13.9828 5.19086 14.4555 5.50628 14.7559C5.84799 15.0814 6.40201 15.0814 6.74372 14.7559L10.5 11.1785L14.2563 14.7559L14.3387 14.8252C14.682 15.0794 15.1783 15.0563 15.4937 14.7559C15.8354 14.4305 15.8354 13.9028 15.4937 13.5774L11.7374 10L15.4937 6.42259L15.5665 6.34408C15.8334 6.01717 15.8091 5.54448 15.4937 5.24408C15.152 4.91864 14.598 4.91864 14.2563 5.24408L10.5 8.82149L6.74372 5.24408L6.66129 5.17475Z" fill="#C9C9C9"/>
                                                            </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" href="#">
                                                    <div class="cartItems">
                                                        <div class="cartItems__products">
                                                            <div class="cartItem__product">
                                                                <div class="cartItem__product--image">
                                                                    <img
                                                                        src="src/images/shoppingCart/mobile.png"
                                                                        alt=""
                                                                        class="img-fluid"
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <p class="cartItem__product--title">3D Implant Planning</p>
                                                                    <p class="cartItem__product--price">$400.00<span>$655.00</span></p>
                                                                </div>
                                                                <div class="checkbox hideDesktop">
                                                                    <label>
                                                                        <input type="checkbox" name="radio4" />
                                                                        <span>3D Printing</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <span class="cartItems__products--close">
                                                            <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.66129 5.17475C6.31803 4.92057 5.82171 4.94367 5.50628 5.24408C5.16457 5.56951 5.16457 6.09715 5.50628 6.42259L9.26256 10L5.50628 13.5774L5.43349 13.6559C5.16659 13.9828 5.19086 14.4555 5.50628 14.7559C5.84799 15.0814 6.40201 15.0814 6.74372 14.7559L10.5 11.1785L14.2563 14.7559L14.3387 14.8252C14.682 15.0794 15.1783 15.0563 15.4937 14.7559C15.8354 14.4305 15.8354 13.9028 15.4937 13.5774L11.7374 10L15.4937 6.42259L15.5665 6.34408C15.8334 6.01717 15.8091 5.54448 15.4937 5.24408C15.152 4.91864 14.598 4.91864 14.2563 5.24408L10.5 8.82149L6.74372 5.24408L6.66129 5.17475Z" fill="#C9C9C9"/>
                                                            </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" href="#">
                                                    <div class="cartItems">
                                                        <div class="cartItems__products">
                                                            <div class="cartItem__product">
                                                                <div class="cartItem__product--image">
                                                                    <img
                                                                        src="src/images/shoppingCart/mobile.png"
                                                                        alt=""
                                                                        class="img-fluid"
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <p class="cartItem__product--title">3D Implant Planning</p>
                                                                    <p class="cartItem__product--price">$400.00<span>$655.00</span></p>
                                                                </div>
                                                                <div class="checkbox hideDesktop">
                                                                    <label>
                                                                        <input type="checkbox" name="radio5" />
                                                                        <span>3D Printing</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <span class="cartItems__products--close">
                                                            <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.66129 5.17475C6.31803 4.92057 5.82171 4.94367 5.50628 5.24408C5.16457 5.56951 5.16457 6.09715 5.50628 6.42259L9.26256 10L5.50628 13.5774L5.43349 13.6559C5.16659 13.9828 5.19086 14.4555 5.50628 14.7559C5.84799 15.0814 6.40201 15.0814 6.74372 14.7559L10.5 11.1785L14.2563 14.7559L14.3387 14.8252C14.682 15.0794 15.1783 15.0563 15.4937 14.7559C15.8354 14.4305 15.8354 13.9028 15.4937 13.5774L11.7374 10L15.4937 6.42259L15.5665 6.34408C15.8334 6.01717 15.8091 5.54448 15.4937 5.24408C15.152 4.91864 14.598 4.91864 14.2563 5.24408L10.5 8.82149L6.74372 5.24408L6.66129 5.17475Z" fill="#C9C9C9"/>
                                                            </svg>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="cartItems__Total">
                                            Total: $1,295.00
                                            <span>$3,276.00</span>
                                            <div class="button-row">
                                                <router-link to="/" class="btn btn-primary full"
                                                    >Go to Cart</router-link
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <!-- <li class="hide-on-lg">
                                    <img
                                        src="src/images/flags/usa.png"
                                        class="img-fluid"
                                        alt="usa"
                                    />
                                </li> -->
                                <li>
                                    <a href="#" class='login' data-toggle="modal" data-target="#login">Login</a>
                                </li>
                                <li class="hide-on-lg">
                                    <a href="#" class='btn btn-primary'>Get Started</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
			</div>
		</header>