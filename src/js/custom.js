$(document).ready(function() {
    voxel.init({

    });
});
var self;
var voxel = {
    init: function(options) {
        this.settings = options;
        self = this;
        this.stickyHeader();
        this.utilities();
        this.sliderFunction();
        this.configureModal();


    },
    stickyHeader: function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 80) {
                $('header').addClass("sticky");
            } else {
                $('header').removeClass("sticky");
            }
        });

    },
    utilities: function() {
        $(".search-icon").click(function(e) {
            e.preventDefault();
            $("li.search form").addClass("search-active");
        });

        // wow animation 
        new WOW().init();

        $(".close-search").click(function(e) {
            e.preventDefault();
            $("li.search form").removeClass("search-active");
        });

        // show vedio pop-up
        $(".mainBanner .howItsWork span").click(function() {
            $(".vedio-popup").fadeIn(300);
            $("body").addClass('');
            $(".header-slider").hide();
            $(".slider-arrows").hide();
        });
        $(".vedio-popup p").click(function() {
            $(".vedio-popup").fadeOut(300);
            $("body").removeClass('');
            $(".header-slider").show();
            $(".slider-arrows").show();
        });
        $(".show-pw").on("click", function(e) {
            e.preventDefault();
            var input = $(this).parent().find(".form-control");
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
            $(this).find('span.eye-icon').toggleClass('eye-slash');
        });

        $(".menu-icon").click(function(e) {
            e.preventDefault();
            $("body").toggleClass("menu-active");
            $("html").toggleClass("no-scroll");
        });

        // inner scroll 
        $(".inner-scroll").mCustomScrollbar();

        //touch spin
        $("input[name='touchSpin']").TouchSpin({
            min: 0,
            max: 5,
            verticalbuttons: false,
        });
        // filter btn
        $(".toggleFilterbtn").on("click", function(e) {
            e.preventDefault();
            if ($(window).width() <= 991) {
                $(this).parent().toggleClass("active");
                $(this).parent().find(".shopFilter").slideToggle();
            }
        });

        $(document).on('hidden.bs.modal', function() {
            $('html').removeClass('no-scroll');
            if ($('.modal.show').length) {
                $('html').addClass('no-scroll');
            }

        });

    },
    // slider functions
    sliderFunction: function() {
        var services = new Swiper('.ourServices .swiper', {
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 1000,
            navigation: {
                "arrows": true,
                "slidesToShow": 1,
                "infinite": true,
                "prevEl": ".ourServices .prevArrow",
                "nextEl": ".ourServices .nextArrow"
            },
            breakpoints: {
                992: {
                    slidesPerView: 5,
                },
                768: {
                    slidesPerView: 4,
                },
                576: {
                    slidesPerView: 3,
                },
                400: {
                    slidesPerView: 2,
                },
            },
        });
        var ourClients = new Swiper('.ourClients .swiper', {
            slidesPerView: 2,
            spaceBetween: 20,
            speed: 1500,
            observer: true,
            observeParents: true,
            loop: true,
            autoplay: {
                delay: 1000,
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                },
                400: {
                    slidesPerView: 1,
                },
            },
        });
        var implantPlaning = new Swiper('.implantPlaning .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 1500,
            navigation: {
                "arrows": true,
                "slidesToShow": 1,
                "infinite": true,
                "prevEl": ".implantPlaning .prevArrow",
                "nextEl": ".implantPlaning .nextArrow"
            },

        });
        var mainbannerSlider = new Swiper('.mainBanner__right .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            effect: 'fade',
            speed: 1500,
            observer: true,
            observeParents: true,
            loop: true,
            autoplay: {
                delay: 1000,
            },
            navigation: {
                "arrows": true,
                "slidesToShow": 1,
                "infinite": true,
                "fade": true,
                "prevEl": ".mainBanner__right .prevArrow",
                "nextEl": ".mainBanner__right .nextArrow"
            },
            fadeEffect: {
                crossFade: true
            },
        });
        let productDetail = new Swiper('.productDetail__relatedProducts .swiper', {
            slidesPerView: 1,
            spaceBetween: 20,
            speed: 1000,
            navigation: {
                "arrows": true,
                "slidesToShow": 4,
                "infinite": true,
                "prevEl": ".productDetail .prevArrow",
                "nextEl": ".productDetail .nextArrow"
            },
            breakpoints: {
                1199: {
                    slidesPerView: 4,
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 10,
                },
                576: {
                    slidesPerView: 2,
                },
                400: {
                    slidesPerView: 1,
                },
            },
        });
        var ourProduct = new Swiper('.productDetailWrapper .swiper', {
            slidesPerView: 1,
            spaceBetween: 0,
            effect: 'fade',
            speed: 1500,
            observer: true,
            observeParents: true,
            loop: true,
            autoplay: {
                delay: 1000,
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            fadeEffect: {
                crossFade: true
            },
        });
    },

    // modal
    configureModal: function() {
        $("body").on("click", "*[data-toggle='custom-modal']", function(e) {
            e.preventDefault();
            $(".custom-modal").removeClass("large");
            var url = $(this).attr("data-path");
            var size = $(this).attr("data-size");
            var class_name = $(this).attr("data-class");
            $(".custom-modal").removeClass("large");
            $(".custom-modal").removeClass("medium");
            $(".custom-modal").removeClass("small");
            $.get(url, function(data) {
                $(".custom-modal").modal("show");
                $(".custom-modal .modal-body").html(data);

                if (size) {
                    $(".custom-modal").addClass(size);
                }
                if (class_name) {
                    $(".custom-modal").addClass(class_name);
                }
                setTimeout(function() {
                    $(".custom-modal .modal-body").addClass("show");
                }, 200);
                $("body").addClass("remove-scroll");
            });
        });
        $(".modal").on("hidden.bs.modal", function() {
            $(".custom-modal .modal-body").removeClass("show");
            $(".custom-modal .modal-body").empty();
            $(".custom-modal").removeClass("account-modal");
            $("body").removeClass("remove-scroll");
            $(".custom-modal").removeClass("large");
            $(".custom-modal").removeClass("medium");
            $(".custom-modal").removeClass("small");
        });
    },

};